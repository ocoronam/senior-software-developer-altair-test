package seniorSoftwareDeveloperTestAltair;

import static seniorSoftwareDeveloperTestAltair.fileUtils.FileUtils.*;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
//import org.apache.log4j.*;

public class WordCounter {
	
	//Nota: la escritura se deberia realizar con un logger log4j2 de apache, 
	//lo incluyo comentado para no tocar la mavenizacion existente, ya que habria que importarlo
	
	//Nota: para la comparacion de vacio y nulo en strings es mejor usar StringUtils.isEmpty() de apache
	
    //private static Logger mylogger;
	
	private static String ELEMENT_OUTPUT = "-  Word: %s  >> Occurrences: %s";
	
	public boolean countWordsInFile(String fileName) { //returns if error
		String word;	
		String msg;
		//get resource
		String filePath = getResourcesFilePath(fileName);
		//get words count
		Map <String,Integer> counterMap = new HashMap<>();
		try ( Scanner input=new Scanner(filePath) ) {
			input.useDelimiter(" +"); //one or more spaces		
						
			Integer c;
			int wordCount=0;
			while(input.hasNext()){
				word = input.next();
				wordCount++;
				if (word==null || word.isEmpty()) {
					msg = String.format("error processing word [%s], PROCESS ABORTED",word);
					System.out.println(msg); //mylogger.error(msg);
					return false;
				}
				//add to counter map
				if (counterMap.containsKey(word)) {
					//inc count
					c = counterMap.get(word) + 1;
					counterMap.replace(word, c);					
				}
				else {
					//add with 1 count
					counterMap.put(word, 1);
				}
			}
			msg = String.format("number of words processed =  [%s]",wordCount);
			System.out.println(msg); //mylogger.debug(msg);
		}
		catch (Exception e) {
			msg = "exception, PROCESS ABORTED";
			System.out.println(msg); //mylogger.error(msg,e);
			return false;
		}
		//print ordered word count
		while (counterMap.size()>0 ) {
			//get actual word with max count in map
			Entry<String, Integer> entry = getWordWithMaxCount(counterMap);
			//print
			msg = String.format(ELEMENT_OUTPUT,entry.getKey(),entry.getValue());
			System.out.println(msg); //mylogger.info(msg);
			//delete entry from map
			counterMap.remove(entry.getKey());
		}		
		return true;
	}

	private static Entry<String, Integer> getWordWithMaxCount(Map<String, Integer> map){ //returns entry (word and count) with max count in map         
		 Entry<String, Integer> maxEntry = null;
	    Integer max = Collections.max(map.values());
	    for(Entry<String, Integer> entry : map.entrySet()) {
	        Integer value = entry.getValue();
	        if(null != value && max == value) {
	            maxEntry = 	entry;
	        }
	    }
	    return maxEntry;
	}
	
	public WordCounter() {
		super();
		//mylogger = Logger.getLogger("MY LOGGER");
	}
	
	
	
	
}
